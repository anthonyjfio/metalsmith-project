var Metalsmith   = require('metalsmith'),
    markdown     = require('metalsmith-markdown'),
    jade         = require('metalsmith-jade'),
    stylus       = require('metalsmith-stylus'),
    templates    = require('metalsmith-templates'),
    permalinks   = require('metalsmith-permalinks'),
    collections  = require('metalsmith-collections'),
    moment       = require('moment');

Metalsmith(__dirname)
  .use(markdown())
  //.use(jade())
  //.use(stylus())
  .use(collections({
    blog: {
      pattern: './blog/*.md',
      sortBy: 'date',
      reverse: 'True'
    }
  }))
  .use(permalinks({
    pattern: ':collections/:title'/*,
    relative: false*/
  }))
  .use(templates('jade'))
  .destination('./build')
  .build(function(err) {
    if (err) {
      throw err; 
    } else {
      console.log("Build Complete");
    }
});
